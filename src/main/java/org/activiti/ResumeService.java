package org.activiti;

import org.springframework.stereotype.Component;

@Component
public class ResumeService {

    public void storeResume() throws Exception {
        System.out.println("Storing Resume ...");
    }

    public void rejectedResume() throws Exception {
      System.out.println("Rejected Resume/Candidate...");
  }
 
    public void acceptedResume() throws Exception {
      System.out.println("Accepted Resume/Candidate...");
  }

}
